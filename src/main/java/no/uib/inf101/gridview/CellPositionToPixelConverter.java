package no.uib.inf101.gridview;

import java.awt.geom.Rectangle2D;

import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.GridDimension;

public class CellPositionToPixelConverter {
  // instansvariablene
  private Rectangle2D box; 
  private GridDimension gd; 
  private double margin; 

  // konstruktør
  public CellPositionToPixelConverter(Rectangle2D box, GridDimension gd, double margin) {
    this.box = box; 
    this.gd = gd;
    this.margin = margin; 
  }

  // metode 
  public Rectangle2D getBoundsForCell(CellPosition cp) {  

    // starter med cellWidth 
    double totalMarginX = margin * (gd.cols() + 1); 
    double leftToCellX = box.getWidth() - totalMarginX; 
    double cellWidth = leftToCellX / gd.cols(); 

    // finner så cellX 
    double cellX = box.getX() + (cp.col()*cellWidth) + ((cp.col() +1)*margin);

    // cellHight 
    double totalMarginY = margin * (gd.rows() +1); 
    double leftToCellY = box.getHeight() - totalMarginY; 
    double cellHight = leftToCellY / gd.rows();

    // cellY 
    double cellY = box.getY() + (cellHight * cp.row())+ (margin * (cp.row() + 1)); 

    return new Rectangle2D.Double(cellX, cellY, cellWidth, cellHight); 


  }
  
}
