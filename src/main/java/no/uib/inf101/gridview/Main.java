package no.uib.inf101.gridview;

import java.awt.Color;

import javax.swing.JFrame;

import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.ColorGrid;

public class Main {
  public static void main(String[] args) {

    // Oppretter et ColorGrid-objekt med 3 rader og 4 kolonner 
    ColorGrid myColorGrid = new ColorGrid(3,4); 

    // Setter fargene i hjørnene 
    myColorGrid.set(new CellPosition(0, 0), Color.RED); 
    myColorGrid.set(new CellPosition(0, 3), Color.BLUE);
    myColorGrid.set(new CellPosition(2, 0), Color.YELLOW);
    myColorGrid.set(new CellPosition(2, 3), Color.GREEN);

    GridView gridView = new GridView(myColorGrid); 
    JFrame frame = new JFrame(); 
    

    frame.setContentPane(gridView);
    frame.setTitle("INF101");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.pack();
    frame.setVisible(true);



  }
}
