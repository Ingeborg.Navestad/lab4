package no.uib.inf101.gridview;

import javax.swing.JPanel;
import no.uib.inf101.colorgrid.CellColorCollection;
import no.uib.inf101.colorgrid.IColorGrid;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

public class GridView extends JPanel {

  private IColorGrid colorGrid;
  private static final double OUTERMARGIN = 30;
  private static final Color MARGINCOLOR = Color.LIGHT_GRAY; 

  
  // konstruktøren 
  public GridView(IColorGrid colorGrid) { 
    this.colorGrid = colorGrid;  
    this.setPreferredSize(new Dimension(400, 300));
  }

 
  @Override
  public void paintComponent(Graphics g) {
    super.paintComponent(g);
    Graphics2D g2 = (Graphics2D) g;
    drawGrid(g2); 
  }


private void drawGrid(Graphics2D g2) {
  // Opprett et Rectangle2D-objekt med en fast 30 pikslers avstand til kanten
  double margin = OUTERMARGIN;
  double x = margin;
  double y = margin;
  double width = getWidth() - 2 * margin;
  double height = getHeight() - 2 * margin;
  Rectangle2D frame = new Rectangle2D.Double(x, y, width, height); 

  // Fyller med gråfarge
  g2.setColor(MARGINCOLOR);
  g2.fill(frame);


  // Opprett et CellPositionToPixelConverter-objekt
  CellPositionToPixelConverter converter = new CellPositionToPixelConverter(frame, colorGrid, OUTERMARGIN);

  // kall til hjelpemetoden drawCells
  drawCells(g2, this.colorGrid, converter); 
	
}

private static void drawCells(Graphics2D g2, CellColorCollection cellColor, CellPositionToPixelConverter converter) {
  cellColor.getCells().forEach(cell -> {
    Rectangle2D bounds = converter.getBoundsForCell(cell.cellPosition());
    g2.setColor(cell.color() != null ? cell.color() : Color.DARK_GRAY);
    g2.fill(bounds);
  });

  
}


}





